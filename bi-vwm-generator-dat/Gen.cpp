#include <iostream>
#include <random>
#include <stdlib.h>
#include <cstdlib>

#include "Gen.h"

#pragma warning(disable:4996)

using namespace std;

CGen::CGen (const string &outputFile)
{
	m_FS.open (outputFile);

	m_FS << "CPU,RAM (GB),HDD (GB),Velikost displeje (\"),Rozliseni displeje,Cena (Kc)" << endl;

	vector<pair<string, double>> tmp;

	tmp.push_back (pair<string, double> ("Intel I3-5100U", 2000.0));
	tmp.push_back (pair<string, double> ("Intel I3-5200U", 2500.0));
	tmp.push_back (pair<string, double> ("Intel I5-5200U", 3000.0));
	tmp.push_back (pair<string, double> ("Intel I5-5500U", 3500.0));
	tmp.push_back (pair<string, double> ("Intel I7-5200U", 4000.0));
	tmp.push_back (pair<string, double> ("Intel I7-5500U", 4500.0));

	tmp.push_back (pair<string, double> ("Intel I3-6100U", 2250.0));
	tmp.push_back (pair<string, double> ("Intel I3-6200U", 2750.0));
	tmp.push_back (pair<string, double> ("Intel I5-6200U", 3250.0));
	tmp.push_back (pair<string, double> ("Intel I5-6500U", 3750.0));
	tmp.push_back (pair<string, double> ("Intel I7-6200U", 4250.0));
	tmp.push_back (pair<string, double> ("Intel I7-6500U", 4750.0));

	tmp.push_back (pair<string, double> ("Intel I3-7100U", 2500.0));
	tmp.push_back (pair<string, double> ("Intel I3-7200U", 3000.0));
	tmp.push_back (pair<string, double> ("Intel I5-7200U", 3500.0));
	tmp.push_back (pair<string, double> ("Intel I5-7500U", 4000.0));
	tmp.push_back (pair<string, double> ("Intel I7-7200U", 4500.0));
	tmp.push_back (pair<string, double> ("Intel I7-7500U", 5000.0));
	m_MetaData.insert (pair < string, vector<pair<string, double>>> ("CPU", tmp));

	tmp.clear ();
	tmp.push_back (pair<string, double> ("2", 300.0));
	tmp.push_back (pair<string, double> ("3", 500.0));
	tmp.push_back (pair<string, double> ("4", 700.0));
	tmp.push_back (pair<string, double> ("6", 1000.0));
	tmp.push_back (pair<string, double> ("8", 1300.0));
	tmp.push_back (pair<string, double> ("12", 1700.0));
	tmp.push_back (pair<string, double> ("16", 2000.0));
	tmp.push_back (pair<string, double> ("24", 3000.0));
	tmp.push_back (pair<string, double> ("32", 3500.0));
	m_MetaData.insert (pair < string, vector<pair<string, double>>> ("RAM", tmp));

	tmp.clear ();
	tmp.push_back (pair<string, double> ("320 HDD", 400.0));
	tmp.push_back (pair<string, double> ("500 HDD", 500.0));
	tmp.push_back (pair<string, double> ("750 HDD", 750.0));
	tmp.push_back (pair<string, double> ("1 000 HDD", 1000.0));
	tmp.push_back (pair<string, double> ("128 SSD", 1000.0));
	tmp.push_back (pair<string, double> ("240 SSD", 2000.0));
	tmp.push_back (pair<string, double> ("256 SSD", 2300.0));
	tmp.push_back (pair<string, double> ("320 SSD", 3500.0));
	tmp.push_back (pair<string, double> ("512 SSD", 6000.0));
	m_MetaData.insert (pair < string, vector<pair<string, double>>> ("HDD", tmp));

	tmp.clear ();
	tmp.push_back (pair<string, double> ("10", 5000.0));
	tmp.push_back (pair<string, double> ("11", 5000.0));
	tmp.push_back (pair<string, double> ("12", 5000.0));
	tmp.push_back (pair<string, double> ("13", 5000.0));
	tmp.push_back (pair<string, double> ("13.3", 5500.0));
	tmp.push_back (pair<string, double> ("14", 6000.0));
	tmp.push_back (pair<string, double> ("15.6", 7000.0));
	tmp.push_back (pair<string, double> ("17.3", 8000.0));
	m_MetaData.insert (pair < string, vector<pair<string, double>>> ("Velikost displeje", tmp));

	tmp.clear ();
	tmp.push_back (pair<string, double> ("1280x800", 5000.0));
	tmp.push_back (pair<string, double> ("1366x768", 5500.0));
	tmp.push_back (pair<string, double> ("1600x900", 5700.0));
	tmp.push_back (pair<string, double> ("1680x1050", 6000.0));
	tmp.push_back (pair<string, double> ("1920x1080", 7000.0));
	tmp.push_back (pair<string, double> ("2560x1440", 7500.0));
	tmp.push_back (pair<string, double> ("2880x1800", 8500.0));
	tmp.push_back (pair<string, double> ("3840x2160", 10000.0));
	m_MetaData.insert (pair < string, vector<pair<string, double>>> ("Rozliseni displeje", tmp));
}

CGen::~CGen (void)
{
	for (auto &thr : m_Threads)
		if (thr.joinable ())
			thr.join ();

	if (m_FS.is_open ())
		m_FS.close ();

	system ("pause");
}

void CGen::threadFoo (const int recordsCount, const int random)
{
	srand (random);

	for (int i = 0; i < recordsCount; i++)
	{
		CRecord tmp = generateOneRecord ();
		{
			lock_guard<mutex> lck (m_Mutex);
			writeRecordIntoOutputFile (tmp);
		}
	}
}

void CGen::generateRecords (const int recordsCount, int thrCnt)
{
	m_Threads.reserve (thrCnt);

	if ((recordsCount % thrCnt) == 0)
		for (int i = 0; i < thrCnt; i++)
			m_Threads.push_back (thread (&CGen::threadFoo, this, recordsCount / thrCnt, rand ()));
	else
	{
		int tmp = recordsCount / thrCnt;
		thrCnt--;

		for (int i = 0; i < thrCnt; i++)
			m_Threads.push_back (thread (&CGen::threadFoo, this, tmp, rand ()));

		m_Threads.push_back (thread (&CGen::threadFoo, this, recordsCount - (thrCnt * tmp), rand ()));
	}
}

CRecord CGen::generateOneRecord (void) const
{
	string cpu, ram, hdd, displaySize, displayResolution;
	double price = 0.0;

	cpu = genOnePartOfRecord ("CPU", price);
	ram = genOnePartOfRecord ("RAM", price);
	hdd = genOnePartOfRecord ("HDD", price);
	displaySize = genOnePartOfRecord ("Velikost displeje", price);
	displayResolution = genOnePartOfRecord ("Rozliseni displeje", price);

	price += (rand () % (int)price / 4) + price / 2;

	return CRecord (cpu, ram, hdd, displaySize, displayResolution, price, ",");
}

const string CGen::genOnePartOfRecord (const string &str, double &price) const
{
	string delimither = ":";
	vector<pair<string, double>> tmp = m_MetaData.find (str)->second;
	const int randomIndex = rand () % tmp.size ();
	string ret = tmp[randomIndex].first;

	ret += delimither + to_string ((double)randomIndex / (tmp.size () - 1));

	for (int i = 0; i < 4; i++)
		ret.pop_back ();

	price += tmp[randomIndex].second;

	return ret;
}

void CGen::writeRecordIntoOutputFile (const CRecord &record)
{
	m_FS << record << endl;
	//cout << record << endl;
}
