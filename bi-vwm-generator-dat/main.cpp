#include <fstream>
#include <iostream>
#include <string>

using namespace std;

#include "Gen.h"

int main (void)
{
	int recordsCount, thrCnt;

	cout << "How many records do you want to generate?" << endl;
	cin >> recordsCount;
	cout << "How many threads do you want?" << endl;
	cin >> thrCnt;

	string outPutFileName = "./bi-vwm-random-data.csv";

	CGen generator (outPutFileName);

	generator.generateRecords (recordsCount, thrCnt);

	return 0;
}