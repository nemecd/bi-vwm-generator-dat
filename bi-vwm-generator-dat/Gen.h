#pragma once
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <mutex>
#include <thread>

#include "Record.h"

using namespace std;

class CGen
{
public:
	CGen (const string &outputFile);
	~CGen (void);

	void generateRecords (const int recordsCount, int thrCnt);

private:
	ofstream m_FS;
	FILE *m_OutputFile;
	map<string, vector<pair<string, double>>> m_MetaData;

	vector<thread> m_Threads;
	mutex m_Mutex;
	mutex m_MutexGen;
	void threadFoo (const int recordsCount, const int random);

	CRecord generateOneRecord (void) const;
	const string genOnePartOfRecord (const string &str, double &price) const;
	void writeRecordIntoOutputFile (const CRecord &record);
};


